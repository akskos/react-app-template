import React from "react";
import ReactDOM from "react-dom";

const Index = function() {
  return (
    <div>
      <h1>React app template</h1>
    </div>
  );
};

ReactDOM.render(<Index />, document.getElementById("index"));
